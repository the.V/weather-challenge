import styles from '../styles/components/DetailsList.module.scss';

export default function DetailsList({ description, wind, humidity, cloudy, colorFixer }) {
    return (
        <>
        <div className={styles.detailsList}>
            <h3 className={(colorFixer === 'Clear' || 'Snow') && styles[colorFixer]}>
                Weather Details
            </h3>
            <div className={styles.description}>
                <h4>Description</h4>
                <p>{description}</p>
            </div>
            <div className={styles.cloudy}>
                <h4>Cloudy</h4>
                <p>{cloudy}%</p>
            </div>
            <div className={styles.humidity}>
                <h4>Humidity</h4>
                <p>{humidity}%</p>
            </div>
            <div className={styles.wind}>
                <h4>Wind</h4>
                <p>{wind}km/h</p>
            </div>
        </div>
        </>
    )
}