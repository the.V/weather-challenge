import { useEffect, useState } from 'react';

export default function HistoryList({ location, selectedLocation }) {
    const [items, setItems] = useState([]) 

    useEffect(
        () => {
            if(location !== '') {
                setItems([...items, location])
            } 
        },
        [location]
    )

    function handleClick(e) {
        selectedLocation(e.target.innerText)
    }
    const temp = [...items];
    const list = temp.length > 4 ? temp.reverse().slice(0, 4) : temp.reverse();

    return (
        <>
            {list &&
                list.map((item, index) => {
                    return (
                        <li onClick={handleClick} key={index}>{item}</li>
                    )
                })
            }
        </>
    )
}