import styles from '../styles/Home.module.scss'
import WeatherApp from './WeatherApp'
import axios from 'axios';


function Home({ data }) {
  return (
    <div className={styles.container}>
      <WeatherApp weatherData={data} />
    </div>
  )
}

export async function getStaticProps() {

  const API_KEY = '2ca404ce29ab86c9d9c620aae824558f';
  const LAT = 35.6944;
  const LON = 51.4215;
// mock cordinates(Tehran)
  const apiUrl = `http://api.openweathermap.org/data/2.5/weather?lat=${LAT}&lon=${LON}&appid=${API_KEY}`
    const res = await axios.get(apiUrl);
    const data = await res.data;
    if(!data) {
      return {
        notFound: true,
      }
    }
    return {
        props: { data }
    }
}
export default Home;