import axios from 'axios';
import { useState } from 'react';
import DetailsList from '../components/DetailsList.js';
import styles from '../styles/Home.module.scss'
import CloudIcon from '@mui/icons-material/Cloud';
import InvertColorsIcon from '@mui/icons-material/InvertColors';
import AcUnitIcon from '@mui/icons-material/AcUnit';
import LightModeIcon from '@mui/icons-material/LightMode';
import ThunderstormIcon from '@mui/icons-material/Thunderstorm';
import SearchIcon from '@mui/icons-material/Search';
import HistoryList from '../components/HistoryList.js';
import ReportIcon from '@mui/icons-material/Report';
import HighlightIcon from '@mui/icons-material/Highlight';


export default function WeatherApp({ weatherData }) {
    const [weather, setWeather] = useState(weatherData.weather[0].main) // { Clouds, Rain, Clear, Snow, Extreme , Haze, Mist, Sand, Smoke }
    const [locationValue, setLocationValue] = useState('');
    const [locationItem, setLocationItem] = useState('');
    const [searchData, setSearchData] = useState(weatherData);

    async function handleSearchLocation() {
        if(locationValue !== '') {
            try {
                const API_KEY = '2ca404ce29ab86c9d9c620aae824558f'
                const res = await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${locationValue}&appid=${API_KEY}`)
                if(res.status === 200) {
                    setSearchData(res.data)
                    setWeather(res.data.weather[0].main)
                    setLocationItem(locationValue)
                    setLocationValue('');
                }
            }
            catch(err) {
                if(err.response.status === 404) {
                    alert('Invalid City Provided!')
                }
            }
        }
    }

    async function handleSelectedLocation(locationName) {
        const API_KEY = '2ca404ce29ab86c9d9c620aae824558f'
        const res = await axios.get(`https://api.openweathermap.org/data/2.5/weather?q=${locationName}&appid=${API_KEY}`)
        setSearchData(res.data)
        setWeather(res.data.weather[0].main)
        setLocationValue('')
    }

    function handleInputValue(e) {
        if(e.key === 'Enter') {
            handleSearchLocation();
        }
    }

    const degCalc = deg => Math.floor(deg - 272.15)
    const temp = degCalc(searchData.main.temp)
    const feelsLikeTemp = degCalc(searchData.main.feels_like)


    const hours = new Date().getUTCHours();
    const minutes = new Date().getUTCMinutes();
    const date = new Date().toDateString();

    const derivedWeather = ['Clouds', 'Rain', 'Clear', 'Snow', 'Extreme', 'Haze', 'Mist', 'Sand']
    const weatherImgChecker = derivedWeather.some(x => x === weather)
    return (
        <>
        <div className={weatherImgChecker ? styles[`${weather}`] : styles.Default}>
            <div className={styles.weatherApp}>
                <div className={styles.main}>
                    <div className={styles.weatherInfo}>
                        <span>{temp}°</span>
                        <div className={styles.locationInfo}>
                            <h3>{searchData.name}</h3>
                            <p>{hours}:{minutes} - {date}</p>
                        </div>
                        <div className={styles.weatherIcon}>
                            {
                            weather === 'Rain' ? <InvertColorsIcon /> :
                            weather === 'Clouds' ? <CloudIcon /> :
                            weather === 'Snow' ? <AcUnitIcon /> :
                            weather === 'Clear' ? <LightModeIcon /> :
                            weather === 'Extreme' ? <ThunderstormIcon /> :
                            weather === 'Haze' ? <ReportIcon /> :
                            weather === 'Mist' && <HighlightIcon />
                            }
                            <h4>{weather}</h4>
                        </div>
                        <p>Feels Like: <legend>{feelsLikeTemp}°</legend></p>
                    </div>
                </div>
                <div className={styles.side}>
                    <div className={styles.searchLocation}>
                        <input
                         onKeyDown={handleInputValue}
                         placeholder='Another Location' 
                         value={locationValue} 
                         onChange={e => setLocationValue(e.target.value)} 
                         />
                        <button onClick={handleSearchLocation}><SearchIcon/></button>
                    </div>
                    <div className={styles.sideDetails}>
                        <div className={styles.historyList}>
                            <ul>
                                <HistoryList location={locationItem} selectedLocation={handleSelectedLocation}/>
                            </ul>
                        </div>
                        <div className={styles.weatherDetails}>
                            <DetailsList 
                            description={searchData.weather[0].description}
                            wind={searchData.wind.speed}
                            cloudy={searchData.clouds.all}
                            humidity={searchData.main.humidity}
                            colorFixer={weather}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}
